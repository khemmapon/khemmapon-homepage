# Changelog

<!-- Date format: YYYY-MM-DD -->

## [1.0.0] - 2022-03-13
### Changed
- Update works information.

## [0.1.2] - 2022-03-05
### Changed
- Update work thumbnail image.

## [0.1.1] - 2022-03-05
### Changed
- Remove Greencard app icon background.

## [0.1.0] - 2022-03-04
### Added
- Push code from local.
