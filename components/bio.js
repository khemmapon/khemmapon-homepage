import { Box } from '@chakra-ui/react'
import styled from '@emotion/styled'

export const BioSection = styled(Box)`
  margin-bottom: 0.5em;
`

export const BioYear = styled.span`
  font-weight: bold;
`

export const BioDetail = styled.p`
  padding-left: 1em;
`
