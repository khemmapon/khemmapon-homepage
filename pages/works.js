import { Container, Heading, SimpleGrid, Divider } from '@chakra-ui/react'

import Layout from '../components/layouts/article'
import Section from '../components/section'
import { WorkGridItem } from '../components/gridItem'

import altitudeJuristicThumbnail from '../public/images/works/altitude-development/altitude_thumbnail.png'
import geeersThumbnail from '../public/images/works/french-startup/geeers_thumbnail.png'
import ickThumbnail from '../public/images/works/icon-kaset/ick_thumbnail.png'
import sellcodaSaleAppThumbnail from '../public/images/works/icon-kaset/sellcoda-sale-app.jpg'
import sellcodaShopAppThumbnail from '../public/images/works/icon-kaset/sellcoda-shop-app.jpg'
import greenCardThumbnail from '../public/images/works/ministry-of-industry/greencard_thumbnail.png'
import pineWealthSolutionThumbnail from '../public/images/works/one-asset-management/pine-wealth_thumbnail.jpg'
import megaHomeThumbnail from '../public/images/works/one-stock-home/mega_home_thumbnail.jpg'
import oneStockHomeThumbnail from '../public/images/works/one-stock-home/onestockhome_thumbnail.jpeg'
import sansiriThumbnail from '../public/images/works/sansiri/sansiri_thumbnail.png'
import rentalForTheHolidaysThumbnail from '../public/images/works/sansiri/rental-for-the-holidays_thumbnail.jpg'
import sansiriFamilyThumbnail from '../public/images/works/sansiri/sansiri-family_thumbnail.jpg'
import koomkahThumbnail from '../public/images/works/scg/koomkah_thumbnail.jpg'
import scgThumbnail from '../public/images/works/scg/scg_thumbnail.jpg'
import readyPlasticThumbnail from '../public/images/works/scg/ready-plastic_thumbnail.jpg'

const Works = () => {
  return (
    <Layout>
      <Container>
        {/* Altitude Development Co., Ltd. */}
        <Heading as={'h3'} fontSize={20} mb={6} mt={6}>
          Altitude Development Co., Ltd.
        </Heading>

        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section>
            <WorkGridItem
              company={'altitude-development'}
              id={'altitude-juristic'}
              title={'Altitude Juristic Application'}
              thumbnail={altitudeJuristicThumbnail}
            ></WorkGridItem>
          </Section>
          <Section>
            <WorkGridItem
              company={'altitude-development'}
              id={'cm360'}
              title={'CM360'}
              thumbnail={altitudeJuristicThumbnail}
            ></WorkGridItem>
          </Section>
        </SimpleGrid>

        {/* French Startup */}
        <Section>
          <Divider my={4} />

          <Heading as="h3" fontSize={20} mb={6}>
            French Startup
          </Heading>
        </Section>

        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section delay={0.2}>
            <WorkGridItem
              company={'french-startup'}
              id="geeers"
              title="Geeers"
              thumbnail={geeersThumbnail}
            ></WorkGridItem>
          </Section>
        </SimpleGrid>

        {/* Icon Kaset Co., Ltd. */}
        <Section delay={0.2}>
          <Divider my={4} />

          <Heading as="h3" fontSize={20} mb={6}>
            Icon Kaset Co., Ltd.
          </Heading>
        </Section>

        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section delay={0.4}>
            <WorkGridItem
              company={'icon-kaset'}
              id="sellcoda-bo-v2"
              title="Sellcoda Back Office V2"
              thumbnail={ickThumbnail}
            ></WorkGridItem>
          </Section>
          <Section delay={0.4}>
            <WorkGridItem
              company={'icon-kaset'}
              id="sellcoda-sale-app-v2"
              title="Sellcoda Sale Application V2"
              thumbnail={sellcodaSaleAppThumbnail}
            ></WorkGridItem>
          </Section>

          <Section delay={0.6}>
            <WorkGridItem
              company={'icon-kaset'}
              id="sellcoda-shop-app-v2"
              title="Sellcoda Shop Application V2"
              thumbnail={sellcodaShopAppThumbnail}
            ></WorkGridItem>
          </Section>
        </SimpleGrid>

        {/* Ministry of Industry */}
        <Section delay={0.6}>
          <Divider my={4} />

          <Heading as="h3" fontSize={20} mb={6}>
            Ministry of Industry
          </Heading>
        </Section>

        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section delay={0.8}>
            <WorkGridItem
              company={'ministry-of-industry'}
              id="greencard"
              title="Greencard Application"
              thumbnail={greenCardThumbnail}
            ></WorkGridItem>
          </Section>
        </SimpleGrid>

        {/* One Asset Management, Ltd. */}
        <Section delay={0.8}>
          <Divider my={4} />

          <Heading as="h3" fontSize={20} mb={6}>
            One Asset Management, Ltd.
          </Heading>
        </Section>

        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section delay={1.0}>
            <WorkGridItem
              company={'one-asset-management'}
              id="pine-wealth-solution"
              title="Pine Wealth Solution Management Software"
              thumbnail={pineWealthSolutionThumbnail}
            ></WorkGridItem>
          </Section>
        </SimpleGrid>

        {/* OneStockHome, Ltd. */}
        <Section delay={1.0}>
          <Divider my={4} />

          <Heading as="h3" fontSize={20} mb={6}>
            OneStockHome, Ltd.
          </Heading>
        </Section>

        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section>
            <WorkGridItem
              company={'one-stock-home'}
              id={'mega-home'}
              title={'Mega Home'}
              thumbnail={megaHomeThumbnail}
            ></WorkGridItem>
          </Section>
          <Section>
            <WorkGridItem
              company={'one-stock-home'}
              id={'one-stock-home'}
              title={'One Stock Home'}
              thumbnail={oneStockHomeThumbnail}
            ></WorkGridItem>
          </Section>
        </SimpleGrid>

        {/* Sansiri Public Co., Ltd. */}
        <Section delay={1.2}>
          <Divider my={4} />

          <Heading as="h3" fontSize={20} mb={6}>
            Sansiri Public Co., Ltd.
          </Heading>
        </Section>

        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section delay={1.2}>
            <WorkGridItem
              company={'sansiri'}
              id="command-center"
              title="Command Center"
              thumbnail={sansiriThumbnail}
            ></WorkGridItem>
          </Section>
          <Section delay={1.2}>
            <WorkGridItem
              company={'sansiri'}
              id="command-center-iot"
              title="Command Center IOT"
              thumbnail={sansiriThumbnail}
            ></WorkGridItem>
          </Section>

          <Section delay={1.4}>
            <WorkGridItem
              company={'sansiri'}
              id="rental-for-the-holidays"
              title="Plus Property application / Rental for the holidays"
              thumbnail={rentalForTheHolidaysThumbnail}
            ></WorkGridItem>
          </Section>
          <Section delay={1.4}>
            <WorkGridItem
              company={'sansiri'}
              id="sansiri-family"
              title="Sansiri Family / Plus Property"
              thumbnail={sansiriFamilyThumbnail}
            ></WorkGridItem>
          </Section>
        </SimpleGrid>

        {/* The Siam Cement Public Co., Ltd. (SCG) */}
        <Section delay={1.4}>
          <Divider my={4} />

          <Heading as="h3" fontSize={20} mb={6}>
            The Siam Cement Public Co., Ltd. (SCG)
          </Heading>
        </Section>

        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section delay={1.6}>
            <WorkGridItem
              company={'scg'}
              id="koomkah"
              title="Circular / Koomkah application"
              thumbnail={koomkahThumbnail}
            ></WorkGridItem>
          </Section>
          <Section delay={1.6}>
            <WorkGridItem
              company={'scg'}
              id="continuous-improvement"
              title="Continuous Improvement"
              thumbnail={scgThumbnail}
            ></WorkGridItem>
          </Section>

          <Section delay={1.8}>
            <WorkGridItem
              company={'scg'}
              id="readyplastic-auction"
              title="Readyplastic Auction"
              thumbnail={readyPlasticThumbnail}
            ></WorkGridItem>
          </Section>
          <Section delay={1.8}>
            <WorkGridItem
              company={'scg'}
              id="readyplastic-e-commerce"
              title="Readyplastic E-Commerce Platform"
              thumbnail={readyPlasticThumbnail}
            ></WorkGridItem>
          </Section>
        </SimpleGrid>
      </Container>
    </Layout>
  )
}

export default Works
