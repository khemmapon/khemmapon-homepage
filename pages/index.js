import {
  Box,
  Button,
  Container,
  Heading,
  Icon,
  Image,
  Link,
  List,
  ListItem
} from '@chakra-ui/react'
import { ChevronRightIcon } from '@chakra-ui/icons'
import NextLink from 'next/link'
import {
  IoLogoGitlab,
  IoLogoGithub,
  IoLogoLinkedin,
  IoLogoReact,
  IoLogoNodejs,
  IoLogoVue,
  IoLogoPython
} from 'react-icons/io5'

import { DiRor, DiPostgresql } from 'react-icons/di'

import { BioSection, BioYear, BioDetail } from '../components/bio'
import Layout from '../components/layouts/article'
import Paragraph from '../components/paragraph'
import Section from '../components/section'

const Page = () => {
  return (
    <Layout>
      <Container>
        <Box display={{ md: 'flex' }} mt={6}>
          <Box flexGrow={1}>
            <Heading as={'h2'} variant={'page-title'}>
              Khemmapon Tawantham
            </Heading>
            <p>Mobile Application and Full Stack Web Developer</p>
          </Box>
          <Box
            flexShrink={0}
            mt={{ base: 4, md: 0 }}
            ml={{ md: 6 }}
            align={'center'}
          >
            <Image
              borderColor={'whiteAlpha.800'}
              borderWidth={2}
              borderStyle={'solid'}
              maxWidth={'100px'}
              display={'inline-block'}
              borderRadius={'full'}
              src={'/images/khemmapon.jpg'}
              alt={'Profile Image'}
            ></Image>
          </Box>
        </Box>

        <Section delay={0.1}>
          <Heading as={'h3'} variant={'section-title'}>
            About
          </Heading>
          <Paragraph>
            Experienced Full Stack and Mobile Application Developer with a
            demonstrated history of working in the information technology and
            services industry. Strong engineering professional with a
            Bachelor&apos;s Degree focused in Multimedia Technology from
            Thai-Nichi Institute of Technology.
          </Paragraph>
        </Section>

        <Section delay={0.2}>
          <Heading as={'h3'} variant={'section-title'}>
            Skills
          </Heading>
          <Paragraph>
            {<Icon as={IoLogoReact}></Icon>} React Native,
            {<Icon as={IoLogoReact}></Icon>} React.js,
            {<Icon as={IoLogoNodejs}></Icon>} Nuxt.js,
            {<Icon as={IoLogoVue}></Icon>} Vue.js,
            {<Icon as={DiRor}></Icon>} Ruby on Rails,
            {<Icon as={IoLogoPython}></Icon>} Python,
            {<Icon as={IoLogoNodejs}></Icon>} Node.js,
            {<Icon as={IoLogoNodejs}></Icon>} NestJS,
            {<Icon as={DiPostgresql}></Icon>} PostgreSQL
          </Paragraph>
          <Box align={'center'} my={4}>
            <NextLink href={'/works'} passHref>
              <Button
                rightIcon={<ChevronRightIcon></ChevronRightIcon>}
                colorScheme={'teal'}
              >
                My portfolio
              </Button>
            </NextLink>
          </Box>
        </Section>

        <Section delay={0.3}>
          <Heading as={'h3'} variant={'section-title'}>
            Bio
          </Heading>
          <BioSection>
            <BioYear>1994</BioYear>
            <BioDetail>Born in Bangkok, Thailand.</BioDetail>
          </BioSection>
          <BioSection>
            <BioYear>2016</BioYear>
            <BioDetail>
              Worked as a Web Developer at Appximus Co., Ltd.
            </BioDetail>
          </BioSection>
          <BioSection>
            <BioYear>2017 - 2018</BioYear>
            <BioDetail>
              Worked as a Full-stack Developer and a Mobile Application
              Developer at FineCoding Co., Ltd.
            </BioDetail>
          </BioSection>
          <BioSection>
            <BioYear>2018 - 2021</BioYear>
            <BioDetail>
              Worked as a Full-stack Developer and a Mobile Application
              Developer at GeekStart Co., Ltd.
            </BioDetail>
          </BioSection>
          <BioSection>
            <BioYear>2021 - 2022</BioYear>
            <BioDetail>
              Worked as a Senior Software Engineer at Icon Kaset Co., Ltd.
            </BioDetail>
          </BioSection>
          <BioSection>
            <BioYear>2022 - present</BioYear>
            <BioDetail>
              Worked as a Senior Software Engineer at OneStockHome Co., Ltd.
            </BioDetail>
          </BioSection>
        </Section>

        <Section delay={0.4}>
          <Heading as={'h3'} variant={'section-title'}>
            On the web
          </Heading>
          <List>
            <ListItem>
              <Link href={'https://github.com/khemmapon'} target={'_blank'}>
                <Button
                  variant={'ghost'}
                  colorScheme={'teal'}
                  leftIcon={<Icon as={IoLogoGithub}></Icon>}
                >
                  @khemmapon
                </Button>
              </Link>
            </ListItem>
            <ListItem>
              <Link href={'https://gitlab.com/khemmapon'} target={'_blank'}>
                <Button
                  variant={'ghost'}
                  colorScheme={'teal'}
                  leftIcon={<Icon as={IoLogoGitlab}></Icon>}
                >
                  @khemmapon
                </Button>
              </Link>
            </ListItem>
            <ListItem>
              <Link
                href={'https://www.linkedin.com/in/khemmapon-tawantham/'}
                target={'_blank'}
              >
                <Button
                  variant={'ghost'}
                  colorScheme={'teal'}
                  leftIcon={<Icon as={IoLogoLinkedin}></Icon>}
                >
                  khemmapon-tawantham
                </Button>
              </Link>
            </ListItem>
          </List>
        </Section>
      </Container>
    </Layout>
  )
}

export default Page
