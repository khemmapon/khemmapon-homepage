import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'

import { Meta, Title } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const SansiriFamily = () => {
  return (
    <Layout title={'Sansiri Family / Plus Property'}>
      <Container>
        <Title>
          Sansiri Family / Plus Property <Badge>2018-2021</Badge>
        </Title>
        <Paragraph>A website for asset management.</Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Website</Meta>
            <Link href={'https://family.sansiri.com/'} target={'_blank'}>
              https://family.sansiri.com/{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Web</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>Ruby on Rails (Backend)</span>
          </ListItem>
        </List>
      </Container>
    </Layout>
  )
}

export default SansiriFamily
