import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'

import { Meta, Title, WorkImage } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const Geeers = () => {
  return (
    <Layout title={'Geeers'}>
      <Container>
        <Title>
          Geeers <Badge>2018-2021</Badge>
        </Title>
        <Paragraph>
          An application for workflow management. It can be integrated with
          external applications.
        </Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Website</Meta>
            <Link href={'https://www.geeers.com/'} target={'_blank'}>
              https://www.geeers.com/{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Web</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>React.js (Frontend), Ruby on Rails (Backend)</span>
          </ListItem>
        </List>

        <WorkImage
          src={'/images/works/french-startup/geeers_01.png'}
          alt={'geeers'}
        ></WorkImage>
        <WorkImage
          src={'/images/works/french-startup/geeers_02.png'}
          alt={'geeers'}
        ></WorkImage>
        <WorkImage
          src={'/images/works/french-startup/geeers_03.png'}
          alt={'geeers'}
        ></WorkImage>
      </Container>
    </Layout>
  )
}

export default Geeers
