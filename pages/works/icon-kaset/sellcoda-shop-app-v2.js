import { Badge, Container, List, ListItem } from '@chakra-ui/react'

import { Meta, Title } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const SellcodaShopApplicationV2 = () => {
  return (
    <Layout title={'Sellcoda Shop Application V2'}>
      <Container>
        <Title>
          Sellcoda Shop Application V2 <Badge>2021-2022</Badge>
        </Title>
        <Paragraph>
          An internal application that manages purchase orders for sales and
          shops.
        </Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Platform</Meta>
            <span>iOS/Android</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>React Native</span>
          </ListItem>
        </List>
      </Container>
    </Layout>
  )
}

export default SellcodaShopApplicationV2
