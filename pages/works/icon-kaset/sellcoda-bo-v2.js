import { Badge, Container, List, ListItem } from '@chakra-ui/react'

import { Meta, Title } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const SellcodaBackOfficeV2 = () => {
  return (
    <Layout title={'sellcoda-back-office-v2'}>
      <Container>
        <Title>
          Sellcoda Back Office V2 <Badge>2021-2022</Badge>
        </Title>
        <Paragraph>
          An internal application that manages purchase orders for sales and
          shops.
        </Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Web</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>React.js (Frontend), NestJS (Backend)</span>
          </ListItem>
        </List>
      </Container>
    </Layout>
  )
}

export default SellcodaBackOfficeV2
