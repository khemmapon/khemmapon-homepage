import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'

import { Meta, Title, WorkImage } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const MegaHome = () => {
  return (
    <Layout title={'Mege Home'}>
      <Container>
        <Title>
          Mege Home <Badge>2022-2022</Badge>
        </Title>
        <Paragraph>E-Commerce web application for home facilities.</Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Website</Meta>
            <Link href={'https://megahome.onestockhome.com/'} target={'_blank'}>
              https://megahome.onestockhome.com/{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Web</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>React.js (Frontend), Ruby on Rails (Backend)</span>
          </ListItem>
        </List>

        <WorkImage
          src={'/images/works/one-stock-home/mega_home_01.png'}
          alt={'one-stock-home-homepage'}
        ></WorkImage>
        <WorkImage
          src={'/images/works/one-stock-home/mega_home_02.png'}
          alt={'one-stock-home-products'}
        ></WorkImage>
      </Container>
    </Layout>
  )
}

export default MegaHome
