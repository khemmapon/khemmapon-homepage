import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'

import { Meta, Title, WorkImage } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const PineWealthSolution = () => {
  return (
    <Layout title={'Pine Wealth Solution Management Software'}>
      <Container>
        <Title>
          Pine Wealth Solution Management Software <Badge>2018-2021</Badge>
        </Title>
        <Paragraph>A website for funds management.</Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Website</Meta>
            <Link
              href={'https://www.pinewealthsolution.com/'}
              target={'_blank'}
            >
              https://www.pinewealthsolution.com/{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Web</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>Nuxt.js (Frontend), Ruby on Rails (Backend)</span>
          </ListItem>
        </List>

        <WorkImage
          src={'/images/works/one-asset-management/pine-wealth_01.png'}
          alt={'pine-wealth-solution-management-software'}
        ></WorkImage>
      </Container>
    </Layout>
  )
}

export default PineWealthSolution
