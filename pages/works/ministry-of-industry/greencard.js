import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'

import { Meta, Title, WorkImage } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const Greencard = () => {
  return (
    <Layout title={'Greencard Application'}>
      <Container>
        <Title>
          Greencard Application <Badge>2018-2021</Badge>
        </Title>
        <Paragraph>
          An application for collecting point by buying at the convenience
          stores. Exchange points for gifts or discounts.
        </Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Website</Meta>
            <Link
              href={
                'https://apps.apple.com/th/app/green-card/id1102801091?l=th'
              }
              target={'_blank'}
            >
              Appstore <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Website</Meta>
            <Link
              href={
                'https://play.google.com/store/apps/details?id=com.telerik.GreenCardV3'
              }
              target={'_blank'}
            >
              Google Play <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>iOS/Android</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>React Native</span>
          </ListItem>
        </List>

        <WorkImage
          src={'/images/works/ministry-of-industry/greencard_01.png'}
          alt={'greencard-application'}
        ></WorkImage>
      </Container>
    </Layout>
  )
}

export default Greencard
