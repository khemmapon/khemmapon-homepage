import { Badge, Container, List, ListItem } from '@chakra-ui/react'

import { Meta, Title } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const ContinuousImprovement = () => {
  return (
    <Layout title={'Continuous Improvement'}>
      <Container>
        <Title>
          Continuous Improvement <Badge>2018-2021</Badge>
        </Title>
        <Paragraph>An internal application for management.</Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Web</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>Ruby on Rails (Backend)</span>
          </ListItem>
        </List>
      </Container>
    </Layout>
  )
}

export default ContinuousImprovement
