import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'

import { Meta, Title } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const Koomkah = () => {
  return (
    <Layout title={'Circular / Koomkah application'}>
      <Container>
        <Title>
          Circular / Koomkah application <Badge>2018-2021</Badge>
        </Title>
        <Paragraph>An application for trash management.</Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Website</Meta>
            <Link
              href={'https://www.youtube.com/watch?v=aKuYh84bNBM'}
              target={'_blank'}
            >
              https://www.youtube.com/watch?v=aKuYh84bNBM{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Website</Meta>
            <Link
              href={'https://www.scg.com/innovation/koomkah/'}
              target={'_blank'}
            >
              https://www.scg.com/innovation/koomkah/{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Website</Meta>
            <Link
              href={
                'https://www.scgchemicals.com/th/products-services/technology-solutions/koomkah'
              }
              target={'_blank'}
            >
              https://www.scgchemicals.com/th/products-services/technology-solutions/koomkah{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Website</Meta>
            <Link
              href={
                'https://www.scg.com/sustainability/circular-economy/en/circular-way-projects/koomkah-app/'
              }
              target={'_blank'}
            >
              https://www.scg.com/sustainability/circular-economy/en/circular-way-projects/koomkah-app/{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>iOS/Android</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>React Native</span>
          </ListItem>
        </List>
      </Container>
    </Layout>
  )
}

export default Koomkah
