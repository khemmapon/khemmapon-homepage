import { Badge, Container, Link, List, ListItem } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'

import { Meta, Title, WorkImage } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const ReadyplasticECommerce = () => {
  return (
    <Layout title={'Readyplastic E-Commerce Platform'}>
      <Container>
        <Title>
          Readyplastic E-Commerce Platform <Badge>2018-2021</Badge>
        </Title>
        <Paragraph>A website for plastic management for e-commerce.</Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Website</Meta>
            <Link
              href={'https://www.readyplastic.com/landing_page'}
              target={'_blank'}
            >
              https://www.readyplastic.com/landing_page{' '}
              <ExternalLinkIcon mx={'2px'}></ExternalLinkIcon>
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Web</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>Ruby on Rails (Frontend & Backend)</span>
          </ListItem>
        </List>

        <WorkImage
          src={'/images/works/scg/ready-plastic_01.png'}
          alt={'readyplastic-auction'}
        ></WorkImage>
      </Container>
    </Layout>
  )
}

export default ReadyplasticECommerce
