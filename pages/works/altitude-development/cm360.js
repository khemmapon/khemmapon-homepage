import { Badge, Container, List, ListItem } from '@chakra-ui/react'

import { Meta, Title } from '../../../components/work'
import Paragraph from '../../../components/paragraph'
import Layout from '../../../components/layouts/article'

const CM360 = () => {
  return (
    <Layout title={'CM360'}>
      <Container>
        <Title>
          CM360 <Badge>2018-2021</Badge>
        </Title>
        <Paragraph>
          An internal application for construction management.
        </Paragraph>
        <List ml={4} my={4}>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Web</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>Nuxt.js (Frontend), Ruby on Rails (Backend)</span>
          </ListItem>
        </List>
      </Container>
    </Layout>
  )
}

export default CM360
